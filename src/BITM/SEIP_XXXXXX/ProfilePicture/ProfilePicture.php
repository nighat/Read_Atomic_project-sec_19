<?php


namespace App\ProfilePicture;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;


class ProfilePicture extends DB
{
    private $id;
    private $name;
    private $fileName;
    private $tmpName;



    public function setData($postData) {
        if(array_key_exists('id',$postData)) {
            $this->id = $postData['id'];
        }


        if(array_key_exists('name',$postData)) {
            $this->name = $postData['name'];

        }

        if(array_key_exists('file_upload',$postData)){
            if(array_key_exists("name",$postData)){
                $this->fileName = $postData['file_upload']['name'];
            }
        }
        if(array_key_exists('file_upload',$postData)){
            if(array_key_exists("name",$postData)){
                $this->tmpName = $postData['file_upload']['tmp_name'];
            }
        }
    }

    public function store() {

        $arrData = array($this->name,$this->fileName);
        $sql = "INSERT INTO profile_picture(name,file_name) VALUES (?,?)";
        $statement = $this->DBH->prepare($sql);
        $result = $statement->execute($arrData);



        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :( ");

        Utility::redirect('index.php');

    }

    public function moveFile() {
        move_uploaded_file($this->tmpName, "uploads/".$this->fileName);
    }

    public function index(){

        $sql = "select * from profile_picture where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from profile_picture where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "select * from profile_picture where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }
}