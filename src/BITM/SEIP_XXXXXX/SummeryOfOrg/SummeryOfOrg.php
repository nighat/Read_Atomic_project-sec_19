<?php


namespace App\SummeryOfOrg;

use App\Message\Message;
use App\Utility\Utility;


use App\Model\Database as DB;
use PDO;

class SummeryOfOrg extends DB
{
    private $id;
    private $name;
    private $summery;

    public function setData($postData) {
        if(array_key_exists('id',$postData)) {
            $this->id = $postData['id'];
        }

        if(array_key_exists('name',$postData)) {
            $this->name = $postData['name'];
        }

        if(array_key_exists('summery',$postData)) {
            $this->summery = $postData['summery'];
        }
    }

    public function store() {
        $arrData = array($this->name,$this->summery);
        $sql = "INSERT INTO summary(name,summery) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);
        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :( ");

        Utility::redirect('index.php');
    }

    public function index(){

        $sql = "select * from summary where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from summary where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "select * from summary where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

}