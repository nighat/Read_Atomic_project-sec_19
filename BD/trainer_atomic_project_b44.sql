-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2017 at 04:24 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `trainer_atomic_project_b44`
--
CREATE DATABASE IF NOT EXISTS `trainer_atomic_project_b44` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `trainer_atomic_project_b44`;

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE IF NOT EXISTS `birthday` (
`id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `date` date NOT NULL,
  `soft_deleted` varchar(1111) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `date`, `soft_deleted`) VALUES
(1, 'vvghh', '2017-02-07', 'no'),
(2, 'bb', '2017-02-07', 'no'),
(3, 'Nighat Parvin', '2017-02-02', 'yes'),
(4, 'Nighat Parvin', '2017-02-05', 'no'),
(5, 'sss', '2017-02-08', 'yes'),
(6, 'Shami', '0000-00-00', 'no'),
(7, '', '0000-00-00', 'no'),
(8, 'ggg', '2017-02-06', 'no'),
(9, 'Nighat Parvin', '2017-02-12', 'no'),
(10, 'Nighat Parvin', '2017-02-12', 'no'),
(11, 'nn', '2017-02-13', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE IF NOT EXISTS `book_title` (
`id` int(11) NOT NULL,
  `book_name` varchar(11111) NOT NULL,
  `author_name` varchar(111) NOT NULL,
  `soft_deleted` varchar(111) DEFAULT 'no'
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_name`, `author_name`, `soft_deleted`) VALUES
(1, 'nsa', 'n', ''),
(2, 'Nhddk', 'rttt', ''),
(3, 'dd', 'dddd  bgg', ''),
(4, 'dd', 'ss', ''),
(5, 'www', 'www', ''),
(6, 'gg', 'gg', ''),
(7, 'fff', 'fff', ''),
(8, 'bin', 'tap', ''),
(9, 'nighat parvion', 'bbbb', ''),
(10, 'hhh', 'hased', 'no'),
(11, 'Nighat Parvin', 'Hossain Book Dhaka', 'no'),
(12, 'Nighat Parvin', 'shamiha', 'yes'),
(13, 'Shamim miha', 'shamiha/shabiha', 'no'),
(14, 'Pinkey', 'shamiha/shabiha', 'yes'),
(15, 'siza', 'shamiha/shabiha', 'no'),
(16, 'Nhddk', 'kkkk', 'no'),
(17, 'nn', 'nn', 'no'),
(18, 'bbbb', 'g', 'no'),
(19, 'NMM', 'JIJ', 'no'),
(20, 'Nighat Parvin', 'bbbb', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
`id` int(111) NOT NULL,
  `name` varchar(11) NOT NULL,
  `city` varchar(111) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city`, `soft_deleted`) VALUES
(1, '', 'Kampong Soam', ''),
(2, '', 'Dhaka', ''),
(3, '', 'Phnom Penh', ''),
(4, '', 'Phnom Penh', ''),
(5, '', 'Kampong Soam', ''),
(6, '', 'Kampong Soam', 'no'),
(7, '', 'Phnom Penh', 'yes'),
(8, '', 'Kampong Soam', 'no'),
(9, '', 'Dhaka', 'yes'),
(10, '', 'Siem Reap', 'no'),
(11, '', 'Siem Reap', 'no'),
(12, '', 'Siem Reap', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
`id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `soft_deleted` varchar(111) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `soft_deleted`) VALUES
(1, 'Nighat Parvin', 'nighatctg@gmail.com', 'no'),
(2, 'hanan', 'nighatctg@gmail.com', 'yes'),
(3, 'pinkey', 'pinkeyctg@gmail.com', 'no'),
(4, 'Shamiha Binty Shamim', 'shamiha@gmail.com', 'yes'),
(5, 'Shamiha Binty Shamim', 'shamiha@gmail.com', 'no'),
(6, 'Shamiha Binty Shamim', 'shamiha@gmail.com', 'yes'),
(7, 'Nighat Parvin', 'dddf2GGT', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
`id` int(11) NOT NULL,
  `name` varchar(11) NOT NULL,
  `gender` varchar(111) NOT NULL,
  `soft_deleted` varchar(111) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender`, `soft_deleted`) VALUES
(1, 'Nighat Parv', 'female', 'yes'),
(2, 'Hasan', 'male', 'no'),
(3, 'Shamiha Bin', 'female', 'yes'),
(4, 'Ruma Binty ', 'female', 'no'),
(5, 'Ruma ', 'female', 'yes'),
(6, 'Sujana', 'female', 'no'),
(7, 'hanan', 'male', 'no'),
(8, 'bbbbb', 'female', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE IF NOT EXISTS `hobbies` (
`id` int(11) NOT NULL,
  `name` varchar(11111) NOT NULL,
  `hobby` varchar(111) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `hobby`, `soft_deleted`) VALUES
(1, 'Nighat Parvin', 'Array', 'no'),
(2, 'Nighat Parvin', 'Reading', 'yes'),
(3, 'Nighat Parvin', 'Painting', 'no'),
(4, 'bbbbb', 'Array', 'no'),
(5, 'bbbbb', 'Array', 'no'),
(6, 'Nighat Parvin', 'Array', 'no'),
(7, 'Shamiha Binty Shamim', 'Painting', 'yes'),
(8, 'new name', 'Playing Guiter,Reading,Painting,Gardenning', 'no'),
(9, '1st', 'Playing Guiter', 'no'),
(10, 'test name', 'Reading', 'no'),
(11, 'Shamiha Binty Shamim', 'Playing Guiter,Reading,Painting', 'no'),
(12, 'bbbbb', 'Playing Guiter,Reading', 'yes'),
(13, 'fF', 'Painting,Gardenning', 'no'),
(14, 'gg', 'Reading,Painting', 'no'),
(15, 'fff', 'Playing Guiter,Reading', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE IF NOT EXISTS `profile_picture` (
`id` int(11) NOT NULL,
  `name` varchar(11111) NOT NULL,
  `file_name` varchar(111) NOT NULL,
  `soft_deleted` varchar(111) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `name`, `file_name`, `soft_deleted`) VALUES
(1, 'n', 'DSC_0126.JPG', 'no'),
(2, 'new name', 'DSC_0101.JPG', 'no'),
(3, 'Shamiha Binty Shamim', 'DSC_0116.JPG', 'no'),
(4, 'upload file in  uploads folder', 'upload.JPG', 'no'),
(5, 'hanan', 'DSC_0534.JPG', 'no'),
(6, 'Shamiha Binty Shamim', 'DSC_0107.JPG', 'yes'),
(7, '1st', 'DSC_0107.JPG', 'no'),
(8, 'Nighat Parvin', 'DSC_0107.JPG', 'yes'),
(9, '1st', 'DSC_0107.JPG', 'no'),
(10, 'Nighat Parvin', 'DSC_0131.JPG', 'no'),
(11, 'Nighat Parvin', 'DSC_0110.JPG', 'no'),
(12, 'Shamiha Binty Shamim', 'Untitled 5 .png', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `summary`
--

CREATE TABLE IF NOT EXISTS `summary` (
`id` int(11) NOT NULL,
  `name` varchar(11) NOT NULL,
  `summery` varchar(11) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary`
--

INSERT INTO `summary` (`id`, `name`, `summery`, `soft_deleted`) VALUES
(1, 'Nighat Parv', 'ddd', 'no'),
(2, 'bb', 'sss', 'yes'),
(3, 'hasan', 'hhjfdjdfjfj', 'yes'),
(4, 'bb', 'bb', 'no'),
(5, 'tt', 't', 'no'),
(6, 'ggh', 'hgh', 'no'),
(7, 'ggh', 'hgh', 'no');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary`
--
ALTER TABLE `summary`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
MODIFY `id` int(111) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `summary`
--
ALTER TABLE `summary`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
